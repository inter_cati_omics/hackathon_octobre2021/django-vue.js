# Atelier Django/Vue.js

## Données

Comparaison de 13 génomes provenant de souches de Xanthomonas campestris : 
https://pubmed.ncbi.nlm.nih.gov/26581393/

Analyse OrthoMCL effectuée sur Family-companion :

https://bbric-pipelines.toulouse.inra.fr/family-companion?view=j4jFHYiBQ5&owner=ludovic.cottret@inrae.fr

On prend 4 groupes orthomcl pour lesquels on récupère les stats, les alignements et les arbres.

## Objectifs 

- Récupérer les données via l'api Django
- Afficher les stats (v-for dans un tableau)
- Afficher les alignements 
  - https://www.npmjs.com/package/vue-svg-msa?activeTab=readme
  - https://www.npmjs.com/package/vue-msa-overview
- Afficher les arbres
  - https://www.npmjs.com/package/vue-phylogram

